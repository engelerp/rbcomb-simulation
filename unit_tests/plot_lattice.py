import matplotlib.pyplot as plt
import numpy as np

lattice = np.loadtxt('lattice.txt', dtype=float)

#plot row by row
for i in range(len(lattice[:,0])):
    plt.plot([lattice[i,0],lattice[i,2]],[lattice[i,1],lattice[i,3]])
plt.ylim([-500.,25000.-500])

plt.xlabel('x coordinate [um]')
plt.ylabel('y coordinate [um]')

plt.savefig('lattice.pdf', dpi=500)
