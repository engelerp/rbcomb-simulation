#include <lattice_generator.hpp>
#include <rbcomb_generator.hpp>
#include <drum.hpp>
#include <drum_parameters.hpp>
#include <drum_variables.hpp>
#include <rk4_buffer.hpp>
#include <vector>
#include <iostream>

int main()
{
	using value_t = double;
	using drum_t = Drum<value_t, DrumParameters<value_t>, DrumVariables<value_t>, RK4Buffer<value_t> >;
	using generator_t = LatticeGenerator<value_t, DrumParameters<value_t>, DrumVariables<value_t>, RK4Buffer<value_t> >;
	using rbgen_t = RbcombGenerator<value_t, DrumParameters<value_t>, DrumVariables<value_t>, RK4Buffer<value_t> >;
	using params_t = DrumVariables<value_t>;

        DrumParameters<value_t> dp (800., 6.28318531E-03, 10., 9.64930905E-01, 10000., 1.0, 1.16883395E-06, Vec2<value_t>(0.,0.), 'A');

	std::vector<generator_t*> gen_vec;
	
	rbgen_t rbgen (20, 10);

	gen_vec.push_back(&rbgen);

	std::pair<std::vector<drum_t>, std::vector<std::vector<int> > > result_pair = gen_vec[0]->operator()(dp);

	for(int i = 0; i < result_pair.second.size(); ++i){
		for(int j = 0; j < result_pair.second[i].size(); ++j){
			if(result_pair.second[i][j] != result_pair.second.size()-1){
				auto drum1 = result_pair.first[i];
				auto drum2 = result_pair.first[result_pair.second[i][j]];

				std::cout << drum1.get_parameters().position.x() << " " << drum1.get_parameters().position.y() << " " << drum2.get_parameters().position.x() << " " << drum2.get_parameters().position.y() << std::endl;
			}
		}
	}

	return 0;
}
