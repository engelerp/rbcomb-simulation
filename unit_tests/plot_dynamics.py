import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt("DYNAMICS.TXT")

times = data[:,0]
xes = data[:,2]
xdots = data[:,3]
Vs = data[:,7]

#extract data for drum 1
t = times[::2471]
x = xes[::2471]
xdot = xdots[::2471]
V = Vs[::2471]/1000.

plt.figure(figsize=(10,10))

plt.subplot(311)
plt.plot(t,x,'-',c='r',label='x', lw=0.7)
plt.xlabel('time [ms]')
plt.ylabel('displacement [um]')
plt.legend()

plt.subplot(312)
plt.plot(t,xdot,'-',c='b',label='xdot', lw=0.7)
plt.xlabel('time [ms]')
plt.ylabel('velocity [um / ms]')
plt.legend()

plt.subplot(313)
plt.plot(t,V,'-',c='g',label='V', lw=0.7)
plt.xlabel('time [ms]')
plt.ylabel('drive [mV]')
plt.legend()
plt.savefig("single_drum_dynamics.pdf", dpi=500)
