#include <system.hpp>
#include <system_parameters.hpp>
#include <driver_simple.hpp>
#include <coupler_simple.hpp>
#include <rbcomb_generator.hpp>
#include <drum.hpp>
#include <drum_parameters.hpp>
#include <drum_variables.hpp>
#include <force_simple.hpp>
#include <rk4_buffer.hpp>
#include <rk4_stepper.hpp>
#include <grabber.hpp>
#include <matrix_element_calculator.hpp>
#include <vector>
#include <utility>
#include <chrono>
#include <iostream>
#include <string>

int main(){
        //simplify syntax
        using value_t = double;
        using params_t = DrumParameters<value_t>;
        using vars_t = DrumVariables<value_t>;
        using buffer_t = RK4Buffer<value_t>;
        using drum_t = Drum<value_t, params_t, vars_t, buffer_t>;
        using coupler_t = CouplerSimple<value_t, drum_t>;
        using driver_t = DriverSimple<value_t, drum_t>;
        using sysparams_t = SystemParameters<coupler_t, driver_t>;
        using force_t = ForceSimple<value_t, params_t, vars_t, buffer_t>;
        using stepper_t = Rk4Stepper<value_t, params_t, vars_t, buffer_t, force_t>;
        using generator_t = RbcombGenerator<value_t, params_t, vars_t, buffer_t>;
        using grabber_t = Grabber<value_t, drum_t>;
        using matelecalc_t = MatrixElementCalculator<value_t, params_t, vars_t, drum_t>;
        using system_t = System<value_t, drum_t, grabber_t, sysparams_t, force_t, coupler_t, driver_t, stepper_t, matelecalc_t>;

        value_t t_end = 1000.; //simulate for 1s
        value_t dt = 0.001; //1us steps, i.e. 100 steps per period

        //generate the lattice
        params_t drum_parameters (800., 6.28318531E-03, 10., 9.64930905E-01, 10000., 1.0, 1.16883395E-06, Vec2<value_t> (0.,0.), 'A');
        generator_t lattice_generator(30,20);
        std::pair<std::vector<drum_t>, std::vector<std::vector<int> > > lat_pair = lattice_generator(drum_parameters);

        std::cout << "Drums in system: " << lat_pair.first.size()-1 << std::endl;

        //generate system parameters, force and stepper
        sysparams_t system_parameters (coupler_t(0.,1.), driver_t(2000.,10.), lat_pair.second);
        force_t force;
        stepper_t stepper;

        //generate grabber
        size_t save_every = 10;
        std::string parameters_file = "PARAMS.TXT";
        std::string adjacency_file = "ADJACENCY.TXT";
        std::string dynamics_file = "DYNAMICS.TXT";
        grabber_t grabber (save_every, parameters_file, adjacency_file, dynamics_file);

        system_t system (t_end, dt, lat_pair.first, stepper, force, system_parameters, grabber);

        //simulation timing
        auto start_time = std::chrono::high_resolution_clock::now();
        system.simulate();
        auto end_time = std::chrono::high_resolution_clock::now();

        std::chrono::duration<double> elapsed = end_time - start_time;

        //print
        std::cout << "Performed " << static_cast<int>(t_end/dt) << " steps in " << elapsed.count() << "s\n";
        std::cout << "(" << elapsed.count() / (t_end/dt) << " s/step)\n";

        std::cout << "Saving data to " << parameters_file << ", " << adjacency_file << " and " << dynamics_file << std::endl;
        system.save();



        return 0;
}
