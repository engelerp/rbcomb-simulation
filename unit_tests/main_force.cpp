#include <force.hpp>
#include <force_simple.hpp>
#include <drum.hpp>
#include <drum_parameters.hpp>
#include <drum_variables.hpp>
#include <rk4_buffer.hpp>
#include <vec2.hpp>

int main(){
        using value_t = double;
        using params_t = DrumParameters<value_t>;
        using vars_t = DrumVariables<value_t>;
        using buffer_t = RK4Buffer<value_t>;

        ForceSimple<value_t, params_t, vars_t, buffer_t> myForce;

        DrumParameters<value_t> dp (800., 6.28318531E-03, 10., 9.64930905E-01, 10000., 1.0, 1.16883395E-06, Vec2<value_t> (0.,0.), 'A');
        Drum<value_t, params_t, vars_t, buffer_t> drum (dp);

        std::cout << "Force: " << myForce(drum.get_parameters(), drum.get_variables()) << std::endl;


        return 0;
}
