#include <drum.hpp>
#include <drum_parameters.hpp>
#include <drum_variables.hpp>
#include <rk4_buffer.hpp>
#include <vec2.hpp>
#include <iostream>

int main(){
        using value_t = double;
        
        DrumParameters<value_t> dp (800., 6.28318531E-03, 10., 9.64930905E-01, 10000., 1.0, 1.16883395E-06, Vec2<value_t>(0.,0.), 'A');
        Drum<value_t, DrumParameters<value_t>, DrumVariables<value_t>, RK4Buffer<value_t> > drum (dp);

        std::cout << dp << std::endl;

        return 0;
}
