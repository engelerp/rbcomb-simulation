#include <vec2.hpp>
#include <iostream>

int main(){
        Vec2<double> v1;
        Vec2<double> v2 (10.,10.);
        Vec2<double> v3 (v2);
        Vec2<double> v4 = v3;

        std::cout << "v1 = " << v1 << "\n" << "v2 = " << v2 << "\n" << "v3 = " << v3 << "\n" << "v4 = " << v4 << std::endl;
        std::cout << "v2.normalized() = " << v2.normalized() << std::endl;
        std::cout << "v2.norm_sq() = " << v2.norm_sq() << ", v2.norm() = " << v2.norm() << std::endl;
        std::cout <<  "v2.rotate(v1+2.*v2+(-4.*v3)+4.*v4/2, 2.*v2.phi()*180./3.141592653589793) = " << v2.rotate(v1+2.*v2+(-4.*v3)+4.*v4/2., 2.*v2.phi()*180./3.141592653589793) << std::endl;
        std::cout << "v2.normalize() = " << v2.normalize() << std::endl;

        return 0;
}
