import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt("DYNAMICS.TXT")

times = data[:,0]
xes = data[:,2]
xdots = data[:,3]
Vs = data[:,7]
ts = data[:,6]

#extract data for drum 1
t = times[::3]
t01 = ts[::3]/1000.
x0 = xes[::3]
xdot0 = xdots[::3]
V0 = Vs[::3]/1000.
x1 = xes[1::3]
xdot1 = xdots[1::3]

plt.figure(figsize=(20,10))

plt.subplot(321)
plt.plot(t,x0,'-',c='r',label='x0', lw=0.7)
plt.xlabel('time [ms]')
plt.ylabel('displacement [um]')
plt.legend()

plt.subplot(323)
plt.plot(t,x1,'-',c='r',label='x1', lw=0.7)
plt.xlabel('time [ms]')
plt.ylabel('displacement [um]')
plt.legend()

plt.subplot(322)
plt.plot(t,xdot0,'-',c='b',label='xdot0', lw=0.7)
plt.xlabel('time [ms]')
plt.ylabel('velocity [um / ms]')
plt.legend()

plt.subplot(324)
plt.plot(t,xdot1,'-',c='b',label='xdot1', lw=0.7)
plt.xlabel('time [ms]')
plt.ylabel('velocity [um / ms]')
plt.legend()

plt.subplot(325)
plt.plot(t,V0,'-',c='g',label='V', lw=0.7)
plt.xlabel('time [ms]')
plt.ylabel('drum 0 drive [mV]')
plt.legend()

plt.subplot(326)
plt.plot(t,t01,'-',c='k',label='t01', lw=0.7)
plt.xlabel('time [ms]')
plt.ylabel('coupling [mV]')
plt.legend()
plt.savefig("coupled_drum_dynamics.pdf", dpi=500)
