#include <vortex.hpp>
#include <cmath>
#include <iostream>

int main(){
        using value_t = double;
        value_t a = 1.0;

        Vortex<value_t> vortex (Vec2<value_t> (0., 0.), 10., 0., 3., a);

        std::cout << "Vortex.position() = " << vortex.position() << std::endl;

        Vec2<value_t> eval (1.,1.);
        Vec2<value_t> s1 (0., -a), s2 (std::sqrt(3.)*a/2., a/2.), s3 (-std::sqrt(3.)*a/2., a/2.);

        std::cout << "Vortex( " << eval << ", " << s1 << " ) = " <<  vortex(eval, s1) << std::endl;
        std::cout << "Vortex( " << eval << ", " << s2 << " ) = " <<  vortex(eval, s2) << std::endl;
        std::cout << "Vortex( " << eval << ", " << s3 << " ) = " <<  vortex(eval, s3) << std::endl;

        return 0;
}
