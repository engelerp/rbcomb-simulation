#ifndef DRIVER_SINGLE_HPP_INCLUDED
#define DRIVER_SINGLE_HPP_INCLUDED
#include <driver.hpp>
#include <vector>
#include <cmath>
#include <iostream>

template<typename value_t, typename drum_t>
class DriverSingle: public Driver<value_t, drum_t>{
        public:
                DriverSingle(const value_t amplitude, const value_t frequency, const value_t holdoff_time, const size_t index)
                        : amplitude_(amplitude), frequency_(frequency), holdoff_time_(holdoff_time), index_(index), time_(0.) {}
                ~DriverSingle() = default;

                void precompute(const value_t t_end, const value_t dt, const std::vector<drum_t>& drum_vec) noexcept final override
                {
                        //precompute all values
                        precomp_.reserve(static_cast<int>(2.*t_end/dt)+2.);
                        for(value_t t_pc = 0.; t_pc <= t_end*1.1; t_pc += dt/2.){
                                precomp_.push_back(std::vector<value_t> ());
                                precomp_.back().reserve(drum_vec.size());
                                for(size_t i = 0; i < drum_vec.size(); ++i){
                                        if(i == index_ && t_pc >= holdoff_time_) [[unlikely]]
                                                precomp_.back().push_back(amplitude_*(std::sin(frequency_*2.*M_PI*(t_pc-holdoff_time_) - M_PI/2.) + 1.));
                                        else
                                                precomp_.back().push_back(0);
                                }
                        }
                        current_ = 0;
                }

                void step(value_t dt) noexcept final override{
                        time_ += dt;
                        ++current_;
                }

                value_t operator()(const size_t drum_index) const noexcept final override{
                        return precomp_[current_][drum_index];
                }

        private:
                size_t index_;
                value_t amplitude_;
                value_t frequency_; //frequency of drive
                value_t holdoff_time_; //initial waiting time
                value_t time_; //current simulation time

                //precomputation variables
                std::vector<std::vector<value_t>> precomp_;
                size_t current_;
};

#endif
