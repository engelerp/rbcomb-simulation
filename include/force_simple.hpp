#ifndef FORCE_SIMPLE_HPP_INCLUDED
#define FORCE_SIMPLE_HPP_INDLUDED
#include <force.hpp>
#include <drum_parameters.hpp>
#include <drum_variables.hpp>
#include <drum.hpp>

//This force is to be used with params_t = DrumParameters, vars_t = DrumVariables.

template <typename value_t, typename params_t, typename vars_t, typename buffer_t>
class ForceSimple: public Force<value_t, params_t, vars_t, buffer_t>{
        public:
                ForceSimple() = default;
                ~ForceSimple() = default;

                value_t operator()(
                                const Drum<value_t, params_t, vars_t, buffer_t>& drum,          //Drum we're calculating the force on
                                const Drum<value_t, params_t, vars_t, buffer_t>& neighbour0,    //Neighbour 0
                                const Drum<value_t, params_t, vars_t, buffer_t>& neighbour1,    //Neighbour 1
                                const Drum<value_t, params_t, vars_t, buffer_t>& neighbour2,    //Neighbour 2
                                const value_t time) const noexcept final override
                {
                        //fetch data
                        //note that no copying is done here, these are all references
                        const params_t& drum_params = drum.get_parameters(); //parameters of drum
                        const vars_t& drum_vars = drum.get_variables();      //variables of drum
                        const vars_t& n0_vars = neighbour0.get_variables();  //variables of neighbour 0
                        const vars_t& n1_vars = neighbour1.get_variables();  //variables of neighbour 1
                        const vars_t& n2_vars = neighbour2.get_variables();  //variables of neighbour 2

                        value_t part1 = - drum_params.omega_sq * drum_vars.x_temp; //Hook restore
                        value_t part2 = - drum_params.c * drum_vars.xdot_temp; //damping
                        value_t part3 =   drum_params.coulomb_prefactor * drum_vars.V*drum_vars.V * (1. + 2.*drum_vars.x_temp / drum_params.gap); //Coulomb drive
                        //interaction DONE: This is probably (slightly) incorrect for now. But it (most likely) has the same complexity as the real deal.
                        value_t part4 = 0.;
                        //DONE: branching can be ommitted if using ni_vars.ti instead of drum_vars.ti. Let's time it.
                        part4 += n0_vars.t0*drum_vars.t0 * (1. + 2.*(drum_vars.x_temp + n0_vars.x_temp) / drum_params.gap);
                        part4 += n1_vars.t1*drum_vars.t1 * (1. + 2.*(drum_vars.x_temp + n1_vars.x_temp) / drum_params.gap);
                        part4 += n2_vars.t2*drum_vars.t2 * (1. + 2.*(drum_vars.x_temp + n2_vars.x_temp) / drum_params.gap);
                        part4 *= drum_params.coulomb_prefactor;

                        return part1 + part2 + part3 + part4;
                }
};


#endif
