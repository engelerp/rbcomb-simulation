#ifndef LINE_GENERATOR_HPP_INCLUDED
#define LINE_GENERATOR_HPP_INCLUDED
#include <lattice_generator.hpp>
#include <utility>
#include <vector>
#include <vec2.hpp>

template <typename value_t, typename params_t, typename vars_t, typename sbuffer_t>
class LineGenerator: public LatticeGenerator<value_t, params_t, vars_t, sbuffer_t> {
        public:
                LineGenerator(size_t length) noexcept : length_(length){}
                LineGenerator() = delete;
                ~LineGenerator() = default;

                //the params_t argument shall contain the position of the first drum that is placed.
                //the last drum (return.first.back()) signifies inexistent neighbours, it is not part of the lattice.
                //It shall be of sublattice 'A'.
                std::pair<std::vector<Drum<value_t, params_t, vars_t, sbuffer_t> >, std::vector<std::vector<int> > > 
                        operator()(const params_t& drum_params) noexcept final override
                {
                        using drum_t = Drum<value_t, params_t, vars_t, sbuffer_t>;

                        //we need drum parameters for A and B sublattices
                        params_t params_A(drum_params);
                        params_t params_B(params_A.a, params_A.c, params_A.f, params_A.m, params_A.Q, params_A.gap, params_A.coulomb_prefactor, params_A.position, 'B'); //generate sublattice 'B' type of parameters

                        //connecting vectors
                        Vec2<value_t> vec_right_up = drum_params.s2;
                        Vec2<value_t> vec_right_dn = -1. * drum_params.s1;

                        //prepare memory
                        drums_.reserve(length_);
                        adjacency_vector_.reserve(length_);

                        //push first drum
                        drums_.push_back(drum_t(params_A));
                        adjacency_vector_.push_back(std::vector<int>(3,-1));

                        //initialize next translation vector and next drum parameters
                        Vec2<value_t> next_tv = vec_right_up;
                        params_t next_params = params_B;

                        //whoever constructs this object with length=0 is an idiot themselves
                        for(size_t i = 0; i < length_-1; ++i){
                                next_params.position = drums_.back().get_parameters().position + next_tv;
                                drums_.push_back(drum_t(next_params));
                                adjacency_vector_.push_back(std::vector<int>(3,-1));
                                if(next_params.sublattice == 'B' || next_params.sublattice == 'b'){
                                        //update adjacency
                                        auto it = adjacency_vector_.end();
                                        --it;
                                        (*it)[2] = std::distance(adjacency_vector_.begin(), it)-1;
                                        --it;
                                        (*it)[2] = std::distance(adjacency_vector_.begin(), it)+1;

                                        //update next parameter and translation vector
                                        next_tv = vec_right_dn;
                                        next_params = params_A;
                                }
                                //if this else doesn't work, it's the user's fault
                                else{
                                        //update adjacency
                                        auto it = adjacency_vector_.end();
                                        --it;
                                        (*it)[1] = std::distance(adjacency_vector_.begin(), it)-1;
                                        --it;
                                        (*it)[1] = std::distance(adjacency_vector_.begin(), it)+1;

                                        //update next parameter and translation vector
                                        next_tv = vec_right_up;
                                        next_params = params_B;
                                }
                        }

                        //push a drum to the end that has no neighbours and signifies the inexistent neighbour
                        drums_.push_back(drum_t(drum_params));
                        adjacency_vector_.push_back(std::vector<int>(3,-1));
                        //make no-neighbours point here
                        for(size_t i = 0; i < adjacency_vector_.size()-1; ++i){
                                for(size_t j = 0; j < 3; ++j){
                                        if(adjacency_vector_[i][j] == -1){
                                                adjacency_vector_[i][j] = drums_.size()-1;
                                        }
                                }
                        }

			return std::pair<std::vector<drum_t>, std::vector<std::vector<int> > > (drums_, adjacency_vector_);
                }

        private:
                size_t length_; //Number of equivalent hexagon rows in x and y direction
                std::vector<Drum<value_t, params_t, vars_t, sbuffer_t> > drums_;
                std::vector<std::vector<int> > adjacency_vector_;
};

#endif
