#ifndef COUPLER_CONSTANT_HPP_INCLUDED
#define COUPLER_CONSTANT_HPP_INCLUDED
#include <coupler.hpp>
#include <vector>
#include <cmath>

template <typename value_t, typename drum_t>
class CouplerConstant: public Coupler<value_t, drum_t>{
        public:
                CouplerConstant(const value_t voltage): voltage_(voltage), time_(0.) {}
                ~CouplerConstant() = default;

                void precompute(const value_t t_end, const value_t dt, const std::vector<drum_t>& drum_vec) noexcept final override { }

                void step(value_t dt) noexcept final override
                {
                        time_ += dt;
                }

                value_t operator()(const size_t drum_index, const size_t neighbour_index) const noexcept final override
                {
                        return voltage_;
                }
        
        private:
                value_t voltage_;
                value_t time_;
};

#endif
