#ifndef DRIVER_SIMPLE_HPP_INCLUDED
#define DRIVER_SIMPLE_HPP_INCLUDED
#include <driver.hpp>
#include <vector>
#include <cmath>

template<typename value_t, typename drum_t>
class DriverSimple: public Driver<value_t, drum_t>{
        public:
                DriverSimple(const value_t amplitude, const value_t frequency): amplitude_(amplitude), frequency_(frequency), time_(0.) {}
                ~DriverSimple() = default;

                void precompute(const value_t t_end, const value_t dt, const std::vector<drum_t>& drum_vec) noexcept final override{ }

                void step(value_t dt) noexcept final override{
                        time_ += dt;
                }

                value_t operator()(const size_t drum_index) const noexcept final override{
                        return amplitude_*(std::sin(frequency_*2.*M_PI*time_) + 1.);
                }

        private:
                value_t amplitude_;
                value_t frequency_; //frequency of drive
                value_t time_; //current simulation time
};

#endif
