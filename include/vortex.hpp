#ifndef VORTEX_HPP_INCLUDED
#define VORTEX_HPP_INCLUDED
#include <vec2.hpp>
#include <cmath>

template <typename value_t>
class Vortex{
        public:
                //constructors
                Vortex(Vec2<value_t> position, value_t l0, value_t alpha, value_t delta, value_t a) noexcept: position_(position), l0_(l0), alpha_(alpha), delta_(delta), K_(value_t(4.*3.141592653589793/(3.*std::sqrt(3)*a)), value_t(0.)) {}
                Vortex(Vec2<value_t> position, const Vortex& o) noexcept: position_(position), l0_(o.l0_), alpha_(o.alpha_), delta_(o.delta_), K_(o.K_) {}
                Vortex() noexcept: position_(value_t(0.), value_t(0.)), l0_(1.), alpha_(0), delta_(1.), K_(value_t(4.*3.141592653589793/(3.*std::sqrt(3))), value_t(0.)) {} //TODO: I may want to delete this as I see no case where this is wanted.
                Vortex(const Vortex&) = default;
                Vortex& operator=(const Vortex&) = default;
                ~Vortex() = default;

                //access
                value_t l0() const noexcept { return l0_; }
                value_t alpha() const noexcept { return alpha_; }
                value_t delta() const noexcept { return delta_; }
                Vec2<value_t>& position() noexcept { return position_; }

                //modifiers
                void set_l0(value_t l0) noexcept { l0_ = l0; }
                void set_alpha(value_t alpha) noexcept { alpha_ = alpha; }
                void set_delta(value_t delta) noexcept { delta_ = delta; }
                void set_position(const Vec2<value_t>& position) noexcept { position_ = position; }
                void move_by(const Vec2<value_t>& translation) noexcept { position_ += translation; }
                void move_to(const Vec2<value_t>& position) noexcept { position_ = position; }

                //functional
                //there is no divide_by_zero check for efficiency reasons. l0_ can not be zero upon call.
                value_t operator()(const Vec2<value_t>& v, const Vec2<value_t>& s)
                {
                        value_t prefactor = delta_*std::tanh(v.r_wrt(position_)/l0_);
                        value_t cosine = std::cos(alpha_ - v.phi_wrt(position_) + K_*(s + 2.*(v - position_)));
                        return prefactor*cosine;
                }
        private:
                Vec2<value_t> position_;
                const Vec2<value_t> K_;
                value_t l0_, alpha_, delta_;
};

#endif
