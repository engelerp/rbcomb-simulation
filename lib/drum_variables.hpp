#ifndef DRUM_VARIABLES_HPP_INCLUDED
#define DRUM_VARIABLES_HPP_INCLUDED

/*
 * Stores all dynamic variables of a drum
 */

//TODO: Generate an interface. It would be trivial, though.
template <typename value_t>
class DrumVariables{
        public:
                //constructors
                DrumVariables() = default;
                DrumVariables& operator=(const DrumVariables&) = default;
                ~DrumVariables() = default;

                value_t t0, t1, t2; //Couplings to neighbours 0, 1, 2
                value_t V; //Coupling to central electrode
                value_t x, xdot; //Current Elongation (position) and velocity
                value_t x_temp, xdot_temp; //used as calculation arguments in rk steps
};

#endif
