#ifndef RK4_BUFFER_HPP_INCLUDED
#define RK4_BUFFER_HPP_INCLUDED

template <typename value_t>
class RK4Buffer{
        public:
                RK4Buffer() = default;
                ~RK4Buffer() = default;

        public:
                value_t k1, k2, k3, k4; //for velocity
                value_t l1, l2, l3, l4; //for position

};

#endif
