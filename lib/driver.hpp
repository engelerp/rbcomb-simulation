#ifndef DRIVER_HPP_INCLUDED
#define DRIVER_HPP_INCLUDED
#include <vector>
#include <drum.hpp>

template <typename value_t, typename drum_t>
class Driver{
        public:
                //constructors
                Driver() = default;
                ~Driver() = default;

                virtual void precompute(const value_t t_end, const value_t dt, const std::vector<drum_t>& drum_vec) noexcept = 0;
                virtual void step(value_t dt) noexcept = 0;
                virtual value_t operator()(const size_t drum_index) const noexcept = 0;
};

#endif
