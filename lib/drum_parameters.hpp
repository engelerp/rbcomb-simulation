#ifndef DRUM_PARAMETERS_HPP_INCLUDED
#define DRUM_PARAMETERS_HPP_INCLUDED
#include <vec2.hpp>
#include <cmath>
#include <iostream>

//TODO: Create an interface. It would be a trivial one, though.
template <typename value_t>
class DrumParameters{
        public:
                /* Arguments:
                 * a:                   horizontal distance to nearest neighbour [um]
                 * c:                   damping constant [kHz]
                 * f:                   resonance frequency [kHz]
                 * m:                   drum mass [mg]
                 * Q:                   drum Q factor
                 * gap:                 gap between wafers [um]
                 * coulomb_prefactor:   k_c * capacitance^2 / m, used for electrostatic force calculation
                 * position:            position of the drum in real space
                 * sublattice:          'A'/'a' or 'B'/'b', identifies sublattice drum is part of
                 */
                DrumParameters(const value_t a, const value_t c, const value_t f, const value_t m, const value_t Q, const value_t gap, const value_t coulomb_prefactor, const Vec2<value_t>& position, char sublattice) noexcept: a(a), c(c), f(f), m(m), Q(Q), gap(gap), coulomb_prefactor(coulomb_prefactor), sublattice(sublattice), position(position)
                { 
                        generate_sublattice(sublattice, a);
                        omega_sq = 2.*M_PI*f*2.*M_PI*f;
                        gapinv_sq = 1./(gap*gap);
                        gapinv_cb = 2./(gap*gap*gap);
                }
                DrumParameters() = delete; //no default initialization allowed
                DrumParameters(const DrumParameters&) = default;
                DrumParameters& operator=(const DrumParameters&) = default;
                ~DrumParameters() = default;

        private:
                int generate_sublattice(char sublattice, value_t lc) noexcept{
                        if(sublattice == 'A' or sublattice == 'a'){
                                s0 = Vec2<value_t> (0, -lc);
                                s1 = Vec2<value_t> (-std::sqrt(3)*lc/2., lc/2.);
                                s2 = Vec2<value_t> (std::sqrt(3)*lc/2., lc/2.);
                                return 0;
                        }
                        else if(sublattice == 'B' or sublattice == 'b'){
                                s0 = -1*(Vec2<value_t> (0, -lc));
                                s1 = -1*(Vec2<value_t> (-std::sqrt(3)*lc/2., lc/2.));
                                s2 = -1*(Vec2<value_t> (std::sqrt(3)*lc/2., lc/2.));
                                return 0;
                        }
                        else
                                return -1; //invalid sublattice identifier
                }

        public:
                value_t a; //distance between nearest neighbours
                value_t c; //damping parameter
                value_t f; //resonance frequency [Hz]
                value_t omega_sq; //resonance frequency [Hz rad]^2
                value_t m; //mass [kg]
                value_t Q; //Q factor
                value_t coulomb_prefactor; //k_c * C^2 / m
                value_t gap; //gap between wafers
                value_t gapinv_sq; // 1./gap^2
                value_t gapinv_cb; // 2./gap^3
                char sublattice; //sublattice the drum is part of (governs electrode geometry)
                Vec2<value_t> s0, s1, s2; //vectors connecting to neighbours, ordered according to drum.hpp
                Vec2<value_t> position; //position of the drum in real space

};

template<typename value_t>
std::ostream& operator<<(std::ostream& os, const DrumParameters<value_t>& dp)
{
        return os << "Sublattice: " << dp.sublattice << ", a: " << dp.a << ", c: " << dp.c << ", f: " << dp.f << ", m: " << dp.m << ", Q: " << dp.Q << std::endl << "s0: " << dp.s0 << ", s1: " << dp.s1 << ", s2: " << dp.s2;

}

#endif
