#ifndef LATTICE_GENERATOR_HPP_INCLUDED
#define LATTICE_GENERATOR_HPP_INCLUDED
#include <utility>
#include <vector>
#include <drum.hpp>

template <typename value_t, typename params_t, typename vars_t, typename sbuffer_t>
class LatticeGenerator{
        public:
                LatticeGenerator() = default;
                ~LatticeGenerator() = default;

                //returns a std::pair of a std::vector of drums in the system and an adjacency vector of size_t.
                //the last drum (.back()) is the drum seen when no neighbour is present.
                //TODO: later, overload with a vector of drum parameters to set each drum's properties individually
                virtual std::pair<std::vector<Drum<value_t, params_t, vars_t, sbuffer_t> >, std::vector<std::vector<int> > > 
                        operator()(const params_t&) noexcept = 0;
};

#endif
