#ifndef SYSTEM_PARAMETERS_HPP_INCLUDED
#define SYSTEM_PARAMETERS_HPP_INCLUDED
#include <vector>

template <typename coupler_t, typename driver_t>
class SystemParameters{
        public:
                //constructors
                SystemParameters(coupler_t coupler, driver_t driver, std::vector< std::vector<int> > adjacency_vector) noexcept: coupler(coupler), driver(driver), adjacency_vector(adjacency_vector) {}

        
        public:
                coupler_t coupler;
                driver_t driver;
                std::vector< std::vector<int> > adjacency_vector;
};

#endif
