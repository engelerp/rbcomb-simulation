#ifndef MATRIX_ELEMENT_CALCULATOR_HPP_INCLUDED
#define MATRIX_ELEMENT_CALCULATOR_HPP_INCLUDED

template <typename value_t, typename params_t, typename vars_t, typename drum_t>
class MatrixElementCalculator{
        public:
                MatrixElementCalculator() = default;
                ~MatrixElementCalculator() = default;

                //diagonal element at (index, index)
                virtual value_t operator()(const size_t index, const std::vector<drum_t>& drums) const noexcept = 0;
                //coupling elements at (index1, index2)
                //for neighbour 0
                virtual value_t operator()(const size_t index1, const size_t index2, const std::vector<drum_t>& drums) const noexcept = 0;
                //for neighbour 1
                virtual value_t operator()(const size_t index1, const size_t index2, const std::vector<drum_t>& drums, const int) const noexcept = 0;
                //for neighbour 2
                virtual value_t operator()(const size_t index1, const size_t index2, const std::vector<drum_t>& drums, const int, const int) const noexcept = 0;
};

#endif
