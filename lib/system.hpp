#ifndef SYSTEM_HPP_INCLUDED
#define SYSTEM_HPP_INCLUDED
#include <vector>
#include <iostream>

template<typename value_t, typename drum_t, typename grabber_t, typename sysparams_t, typename force_t, typename coupler_t, typename driver_t, typename stepper_t, typename matelecalc_t>
class System{
        public:
                System(const value_t t_end, const value_t dt, const std::vector<drum_t>& drums, const stepper_t stepper, const force_t force, const sysparams_t sysparams, const grabber_t grabber)
                        : drums_(drums), stepper_(stepper), force_(force), sysparams_(sysparams), grabber_(grabber), t_end_(t_end), dt_(dt), time_(0.)
                {
                        sysparams_.coupler.precompute(t_end, dt, drums);
                        sysparams_.driver.precompute(t_end, dt, drums);
                        grabber_.init(t_end, dt, drums, sysparams.adjacency_vector);

                        push_dc(); //push the initial values

                }
                ~System() = default;

                void simulate(){
                        while(time_ <= t_end_){
                                grabber_.grab(drums_, time_);
                                step();
                        }
                }

                void step(){
                        push_dc();
                        stepper_.step_1(force_, drums_, sysparams_.adjacency_vector, dt_, time_);
                        step_dc(dt_/2.);
                        push_dc();
                        stepper_.step_2(force_, drums_, sysparams_.adjacency_vector, dt_, time_);
                        step_dc(dt_/2.);
                        push_dc();
                        stepper_.step_3(force_, drums_, sysparams_.adjacency_vector, dt_, time_);

                        time_ += dt_;
                }

                void reset_time(){
                        time_ = value_t(0.);
                }

                void set_step(value_t dt){
                        dt_ = dt;
                }

                bool save(){
                        return grabber_.save();
                }

                std::vector<value_t> get_matrix(const matelecalc_t& mec){
                        calculate_matrix();
                        return matrix_;
                }

        private:
                void step_dc(const value_t dt){
                        sysparams_.coupler.step(dt);
                        sysparams_.driver.step(dt);
                }

                void push_dc(){
                        for(size_t i = 0; i < drums_.size()-1; ++i){
                                drums_[i].get_variables().V = sysparams_.driver(i);
                                drums_[i].get_variables().t0 = sysparams_.coupler(i,0);
                                drums_[i].get_variables().t1 = sysparams_.coupler(i,1);
                                drums_[i].get_variables().t2 = sysparams_.coupler(i,2);
                        }
                }

                //Calculates dynamical matrix for the currently pushed values
                void calculate_matrix(const matelecalc_t& mec){
                        //for convenience
                        size_t N = drums_.size()-1;
                        //clear the matrix
                        matrix_.clear();
                        //prepare memory
                        matrix_.reserve((drums_.size()-1)*(drums_.size()-1) - matrix_.capacity());
                        //initialize
                        for(size_t i = 0; i < (drums_.size()-1) * (drums_.size()-1); ++i)
                                matrix_.push_back(value_t(0.));

                        for(size_t i = 0; i < drums_.size()-1; ++i){
                                size_t n0_index = sysparams_.adjacency_vector[i][0];
                                size_t n1_index = sysparams_.adjacency_vector[i][1];
                                size_t n2_index = sysparams_.adjacency_vector[i][2];
                                //diagonal
                                matrix_[N*i + i] = mec(i, drums_);
                                //couplings
                                //neighbour 0
                                if(n0_index != drums_.size()-1) [[likely]] //actually a neighbour
                                        matrix_[N*i + n0_index] = mec(i, n0_index, drums_);
                                //neighbour 1
                                if(n1_index != drums_.size()-1) [[likely]] //actually a neighbour
                                        matrix_[N*i + n1_index] = mec(i, n1_index, drums_, 1);
                                //neighbour 2
                                if(n2_index != drums_.size()-1) [[likely]] //actually a neighbour
                                        matrix_[N*i + n2_index] = mec(i, n2_index, drums_, 2, 2);
                        }
                }

                std::vector<drum_t> drums_; //The last drum is NOT TO BE TOUCHED!
                sysparams_t sysparams_;
                grabber_t grabber_;
                stepper_t stepper_;
                force_t force_;
                value_t dt_, t_end_;
                value_t time_;

                std::vector<value_t> matrix_; //Matrix of this problem
};

#endif
