#ifndef FORCE_HPP_INCLUDED
#define FORCE_HPP_INCLUDED
#include <drum.hpp>

//Force evaluation interface
template <typename value_t, typename params_t, typename vars_t, typename buffer_t>
class Force{
        public:
                //constructors
                Force() = default;
                ~Force() = default;

                //virtual functional
                virtual value_t operator()(
                                const Drum<value_t, params_t, vars_t, buffer_t>& drum,          //Drum we're calculating the force on
                                const Drum<value_t, params_t, vars_t, buffer_t>& neighbour0,    //Neighbour 0
                                const Drum<value_t, params_t, vars_t, buffer_t>& neighbour1,    //Neighbour 1
                                const Drum<value_t, params_t, vars_t, buffer_t>& neighbour2,    //Neighbour 2
                                const value_t time) const noexcept = 0;
};

#endif
