<a name="RSF"></a>
# RBComb simulation framework
This project aims at developing a framework that can be used to simulate any project
that is to be undertaken on the RBComb platform. It is designed in modular fashion,
such that it is flexible enough to adapt easily to any given situation. The list of
contents is the following:

1. [ RBComb simulation framework. ](#RSF)
2. [ Prerequisites. ](#prerequisites)
2. [ Unit tests](#UT)
3. [ Implementing a new project. ](#INP)
4. [ Classes. ](#Classes)
   - [ Vec2. ](#vec2)
   - [ Diagonalizer. ](#diagonalizer)
   - [ DrumParameters. ](#drumparams)
   - [ DrumVariables. ](#drumvars)
   - [ Drum. ](#drum)
   - [ Force. ](#force)
   - [ Driver. ](#driver)
   - [ Coupler. ](#coupler)
   - [ LatticeGenerator. ](#latticegenerator)
     - [ Neighbour Ordering Convention. ](#noc)
     - [ Inexistent Neighbour Convention. ](#inc)
   - [ MatrixElementCalculator. ](#mec)
   - [ RK4Buffer. ](#rk4b)
   - [ Rk4Stepper. ](#rk4s)
   - [ SystemParameters. ](#sysparams)
   - [ Grabber. ](#grabber)
   - [ System. ](#system)

<a name="prerequisites"></a>
## Prerequisites
In order to work with the framework provided here, the following prerequisites are
required
- Make
- LAPACK (for the `Diagonalizer`)
  - On OSX this is provided (automagically?) through the framework `Accelerate`
- gcc-9 or newer (for C++2a features like efficient branching)
  - The only other compiler supporting necessary C++2a features at the time of writing is `EDG eccp`.
- python 3

<a name="UT"></a>
## Unit tests
Unit tests are available in the subfolder `unit_tests`. They can be run from within
that directory by issuing `make run`. They should all compile and run fine.


<a name="INP"></a>
## Implementing a new project
In order to implement a new project, follow these steps:

0. Create a new folder in `projects`.
   - Create subfolders `bin`, `include`, `lib` and `results`
   - Copy the `lib` contents into `projects/newproj/lib`
1. Identify force on a single drum
   - Depends on static parameters (w, m, a, etc.) and dynamic parameters and variables (x, v, etc.).
   - Coupling and driving parameters are dynamic.
2. Write custom versions of classes `DrumParameters` (everything that is static) and `DrumVariables`
(everything that is dynamic) to accomodate these. Inspiration in `lib/drum_variables.hpp` and
`lib/drum_parameters.hpp`. Take into account the information provided [here](#drumparams) and [here](#drumvars).
3. Write custom `Grabber` that extracts the relevant data (inspiration in `include/grabber.hpp`).
Take into account what's stated [here](#grabber).
4. Write custom child of `Force` to calculate the force. For more information, have a look [here](#force).
5. Write custom children of `Coupler` and `Driver` to correctly update the drive and couplings.
Remember to read [this](#coupler) and [this](#driver)
6. If desired, write a custom child of `LatticeGenerator` to generate a custom lattice (have a look [here](#latticegenerator)),
or use the generator `projects/braidingTightBinding/include/rbcomb_generator_braid.hpp` to
create the RBComb. When working with lattices, always keep in mind the [neighbour ordering convention](#noc)
and the [inexistent neighbour convention](#inc).
7. If desired, write a child of `MatrixElementCalculator` to calculate matrix elements
for the dynamic matrix. See more information [here](#mec).
8. Check the [unit tests](#UT) or other projects to see how a `main.cpp` could be
constructed.

Hints for organizing, building and running simulations
1. Use a Makefile.
   - The framework should be compiled with c++ 2a.
   - The `Diagonalizer` requires LAPACK to be linked.
   - Get inspiration from other projects and [unit tests](#UT).
2. Organize executables in `bin`
3. Store data and plots in subfolders of `results`



<a name="Classes"></a>
## Classes
The code is structured in an object oriented approach. The classes that likely will
not need to be adapted for a new situation are found in the `lib` folder. They are
described in the following. Note that qualifiers, references and the like are discarded
where it improves legibility. Consult the source files for more information.

<a name="vec2"></a>
#### `Vec2` (vec2.hpp), 2-vector utility class
1. Template arguments
   - `value_t`: type of vector entries
2. Explicit constructors
   - `Vec2(const value_t, const value_t)`
   - `Vec2(const Vec2&)`
   - `Vec2()`
   - `Vec2& operator=(const Vec2&)`
3. Members
   - Access
     - `value_t x()`
       - returns x entry
     - `value_t y()`
       - returns y entry
     - `Vec2 normalized()`
       - returns normalized version of vector
     - `value_t r()`
       - returns length
     - `value_t phi()`
       - returns angle (`std::atan2` version of it)
   - Member functions
     - `value_t r_wrt(Vec2)`
       - returns length with origin at argument
     - `value_t phi_wrt(Vec2)`
       - returns angle with origin at argument
     - `value_t norm()`
       - returns norm
     - `value_t norm_sq()`
       - returns square of norm
   - Modifiers
     - `Vec2 normalize()`
       - normalizes the vector and returns it
       - throws when zero-vector
     - `Vec2 rotate(Vec2, value_t)`
       - rotates the vector and returns it
   - Supported Operators, all of these work as one would expect
     - `*` with `Vec2` (inner product) and `value_t`
     - `/` with `value_t`
       - throws upon division by zero
     - `+, -` with `Vec2`
     - All versions of `op=` of the above
     - `[]` with `std::size_t`
     - `<<` with `std::ostream`
4. Dependents
   - `DrumParameters`
   - `LatticeGenerator` children
5. Dependencies
   - None

<a name="diagonalizer"></a>
#### `Diagonalizer` (diagonalizer.hpp), class to diagonalize symmetric Matrices
1. Explicit constructors
   - `Diagonalizer()`
2. Member functions
   - `std::vector<double> ev(std::vector<double> mat, size_t N)`
     - returns eigenvalues of the symmetric matrix mat of linear size N
     - throws upon diagonalization failure
   - `std::pair<std::vector<double>, std::vector<double> > evv(std::vector<double> mat, size_t N)`
     - returns pair of (eigenvalues, eigenvectors) of the symmetric matrix mat of size N
     - throws upon diagonalization failure
3. Further developments
   - Only finding eigenvectors and -values in a certain range may be added later on
4. Dependents
   - `System`
5. Dependencies
   - None
6. Notes
   - Matrices are assumed in column-major format. For symmetric matrices this doesn't matter.
   - Eigenvectors are returned in form of a matrix, with the eigenvectors in the columns.

<a name="drumparams"></a>
#### `DrumParameters`(drum_parameters.hpp), describes static state of a drum
1. Explicit constructors
   - Determined by implementation
2. Member functions
   - None or determined by implementation
3. Public data members
   - Determined by implementation
4. Typical dependents
   - `Driver`
   - `Coupler`
   - `Force` children
   - `Grabber`
   - `MatrixElementCalculator` children
   - `LatticeGenerator` children
5. Typical dependencies
   - `Vec2`

<a name="drumvars"></a>
#### `DrumVariables` (drum_variables.hpp), describes dynamic state of a drum
1. Explicit constructors
   - Determined by implementation
2. Member functions
   - None or determined by implementation
3. Public data members
   - Determined by implementation
   - Must include (required by [System](#system) and [Drum](#drum))
     - Central drive coupling `value_t V`
     - Neighbour couplings `value_t t0`, `value_t t1`, `value_t t2`
   - Must include (required by [Rk4Stepper](#rk4s))
     - current displacement `value_t x`
     - current displacement storage `value_t x_temp`
     - current velocity `value_t xdot`
     - current velocity storage `value_t xdot_temp`
     - `var` and `var_temp` must be initialized to the same value.
4. Typical dependents
   - `Driver`
   - `Coupler`
   - `Force` children
   - `Grabber`
   - `MatrixElementCalculator` children
   - `System` through private push_dc()
   - `Drum` (see [this issue](https://gitlab.phys.ethz.ch/engelerp/rbcomb-simulation/issues/4)).
5. Typical dependencies
   - `Vec2`
6. Remarks
   - Respect the constraints given by [this issue](https://gitlab.phys.ethz.ch/engelerp/rbcomb-simulation/issues/4).

<a name="drum"></a>
#### `Drum` (drum.hpp), represents a single drum top resonator
1. Template arguments
   - `value_t`: Scalar type
   - `params_t`: Drum parameters container type
   - `vars_t`: Drum variables container type
   - `sbuffer_t`: Stepper buffer container type
2. Explicit constructors
   - `Drum(const params_t&)`
   - `Drum() = delete`
   - `Drum(const Drum&)`
   - `Drum& operator=(const Drum&)`
3. Access
   - `params_t get_parameters()`
     - returns the parameters, const and reference versions implemented
   - `vars_t get_variables()`
     - returns the variables, const and reference versions implemented
   - `sbuffer_t get_sbuffer()`
     - returns the stepper buffer, const and reference versions implemented
4. Modifiers
   - `void set_coupling_0(value_t)`
     - Sets coupling 0
     - [deprecated](https://gitlab.phys.ethz.ch/engelerp/rbcomb-simulation/issues/4)
   - `void set_coupling_1(value_t)`
     - Sets coupling 1
     - [deprecated](https://gitlab.phys.ethz.ch/engelerp/rbcomb-simulation/issues/4)
   - `void set_coupling_2(value_t)`
     - Sets coupling 2
     - [deprecated](https://gitlab.phys.ethz.ch/engelerp/rbcomb-simulation/issues/4)
   - `void set_drive(value_t)`
     - Sets central electrode coupling
     - [deprecated](https://gitlab.phys.ethz.ch/engelerp/rbcomb-simulation/issues/4)
5. Dependents
   - `Coupler` children
   - `Driver` children
   - `Force` children
   - `LatticeGenerator` children
   - `MatrixElementCalculator` children
   - `Rk4Stepper`
   - `System`
   - `Grabber`
6. Dependencies
   - None
7. Description
   - A drum is described by a set of (static) parameters (stiffness, mass, x-y position, etc),
which are to be stored in a container of type `params_t`. The variables (displacement, velocity,
electrode charges, etc.) are stored in a container of type `vars_t`. Example classes for
these two types are `lib/drum_parameters.hpp` and `lib/drum_variables.hpp`.
However, these containers likely need to be adapted to the situation at hand.
When time evolving, the stepper will use the container of
type `sbuffer_t` to store its intermediate results. Note that the default constructor of
this class is `delete`'d. It should be constructed from an object of type `params_t`.
8. Further developments
   - Abstract interfaces for `params_t` and `vars_t` could be added, but they would
be trivial.

<a name="force"></a>
#### `Force` (force.hpp), force functional
1. Template arguments
   - `value_t`: Scalar type
   - `params_t`: Drum parameters type
   - `vars_t`: Drum variables type
   - `buffer_t`: Stepper buffer type
2. Explicit constructors
   - `Force()`
3. Virtual functions
   - `value_t operator()(drum_t drum, drum_t n1, drum_t n2, drum_t n3, value_t time)`
     - Returns force on `drum` at `time`, given its three neighbours `n1`, `n2`, `n3`
4. Typical dependents
   - `Rk4Stepper`, but only virtually
5. Typical dependencies
   - `Drum`
   - `params_t`
   - `vars_t`
6. Description
   - This interface is a guide to complete implementation of a force functional. Any force
functional should derive from this class, but the child type should then be used in
order to avoid the vtable penalty.
   - The type `drum_t` is a `Drum` with the given
template arguments. Typically, this functional would make heavy use of the `Drum`
access members `get_parameters()` and `get_variables()`. The `time` argument of the
functional exists to fit special cases as well. The file `include/force_simple.hpp`
showcases how a real force functional could be written.

<a name="driver"></a>
#### `Driver` (driver.hpp), calculate drive of drums
1. Template arguments
   - `value_t`: Scalar type
   - `drum_t`: Drum type
2. Explicit constructors
   - `Driver()`
3. Virtual functions
   - `void precompute(value_t t_end, value_t dt, std::vector<drum_t> drum_vec)`
     - Called once at begin of `System` lifetime
   - `void step(value_t dt)`
     - Move in time by `dt`
   - `value_t operator()(size_t drum_index)`
     - Returns drive of drum `drum_index` (wrt `drum_vec`) at current time
4. Typical dependents
   - `System`, but only virtually
3. Typical dependencies
   - `drum_t`
   - `DrumParameters`
   - `DrumVariables`
5. Description
   - This interface is a guide to complete implementation of a drive calculation class. Any driver
class should derive from this class, but the child type should then be used in
order to avoid the vtable penalty.
   - The purpose of this class is to set the drive of each drum at specific times.
   - In the `precompute` function, this class is passed all information it could need about
the system. Hence it can in principle precompute all values for all drums and all times
of the simulation.
   - The member `step` is called to inform the `Driver` that time is advanced by the passed argument.
Note that an rk4 scheme advances time in twinned steps of `dt/2`.
   - The functional should return the current drive on the drum with index passed as argument.
   - An example implementation of a `Driver` is shown in `include/driver_simple.hpp`.

<a name="coupler"></a>
#### `Coupler` (coupler.hpp), calculate couplings between drums
1. Template arguments
   - `value_t`: Scalar type
   - `drum_t`: Drum type
2. Explicit constructors
   - `Coupler()`
3. Virtual functions
   - `void precompute(value_t t_end, value_t dt, std::vector<drum_t> drum_vec)`
     - Called once at begin of `System` lifetime
   - `void step(value_t dt)`
     - Move in time by `dt`
   - `value_t operator()(size_t drum_index, size_t neighbour_index)`
     - Returns coupling between drums `drum_index` and `neighbour_index` (wrt `drum_vec`) at current time
4. Typical dependents
   - `System`, but only virtually
3. Typical dependencies
   - `drum_t`
   - `DrumParameters`
   - `DrumVariables`
5. Description
   - This interface is a guide to complete implementation of a coupling calculation class. Any coupler
class should derive from this class, but the child type should then be used in
order to avoid the vtable penalty.
   - The purpose of this class is to set the coupling of each neighbouring pair of drums at specific times.
   - In the `precompute` function, this class is passed all information it could need about
the system. Hence it can in principle precompute all values for all drums and all times
of the simulation.
   - The member `step` is called to inform the `Coupler` that time is advanced by the passed argument.
Note that an rk4 scheme advances time in twinned steps of `dt/2`.
   - The functional should return the current coupling between the two drums with indices passed as arguments.
   - An example implementation of a `Coupler` is shown in `include/coupler_simple.hpp`.

<a name="latticegenerator"></a>
#### `LatticeGenerator` (lattice_generator.hpp), generates drum lattices
1. Template arguments
   - `value_t`: Scalar type
   - `params_t`: Drum parameters type
   - `vars_t`: Drum variables type
   - `sbuffer_t`: Stepper buffer type
2. Explicit constructors
   - `LatticeGenerator()`
3. Virtual functions
   - `std::pair<std::vector<drum_t>, std::vector<int> > operator()(params_t)`
     - Takes a `params_t`
     - Returns a pair that characterizes the generated lattice
       - a vector of drums `ds`
       - an adjacency vector of vectors `adj`, such that `ds[i]` and `ds[adj[i][0]]` are neighbours
     - All drums have the same `params_t`, except that the `position` members differ.
4. Description
   - An example child of the `LatticeGenerator` is shown in the file `include/rbcomb_generator.hpp`.
5. Further developments
   - In the future, there may be another overload for the functional. For example, it could either take an
`std::vector<params_t>` or an additional random number generator to construct the drums
differently.
6. Dependents
   - None
7. Typical dependencies
   - `params_t` (existence of `position` member)
<a name="noc"></a>
##### Neighbour ordering convention
An important note is the __convention of neighbour ordering__. Each drum has neighbours 0 thru 3.
For drums in different sublattices, these neighbours are:
- Sublattice 'A':
  - 0, `adj[0]`: straight down
  - 1, `adj[1]`: top left
  - 2, `adj[2]`: top right
- Sublattice 'B':
  - 0, `adj[0]`: straight up
  - 1, `adj[1]`: bottom right
  - 2, `adj[2]`: bottom left
Here _adj[]_ signifies the adjacency list of the given drum. Similarly, the couplings
_t0_ thru _t2_ in objects of type `params_t` should also respect this ordering. More
generally, whenever neighbours of a specific drum are ordered in some fashion, they
are assumed to respect the above convention. Note that with this convention,
neighbours see each other as the same neighbour index (the i-th neighbour of j
  sees j as its i-th neighbour). __Never violate this convention__.
<a name="inc"></a>
##### Inexistent neighbour convention
Another __convention__ concerns __inexistent neighbours__. For that purpose, this class should append
an auxiliary drum to the end of the drum vector. If neighbour i of a drum does not exist
(due to boundary, for example), the corresponding neighbour index will point to the
auxiliary drum, i.e. it will show an index `drum_vec.size()`. All couplings of this auxiliary
drum are to be kept at 0. This condition can then be applied in force calculation to avoid branching,
if one uses the couplings of the neighbours instead of the considered drum.

<a name="mec"></a>
#### `MatrixElementCalculator` (matrix_element_calculator.hpp), calculates matrix elements
1. Template arguments
   - `value_t`: Scalar type
   - `params_t`: Drum parameters type
   - `vars_t`: Drum variables type
   - `drum_t`: Drum type
2. Explicit constructors
   - `MatrixElementCalculator()`
3. Virtual functions
   - `value_t operator()(size_t index, std::vector<drum_t>)`
     - Returns the diagonal element (index, index).
   - `value_t operator()(size_t index1, size_t index2, std::vector<drum_t>)`
     - Returns the coupling element (index1, index2), where these are each others neighbour 0
   - `value_t operator()(size_t index1, size_t index2, std::vector<drum_t>, int)`
     - Returns the coupling element (index1, index2), where these are each others neighbour 1
   - `value_t operator()(size_t index1, size_t index2, std::vector<drum_t>, int, int)`
     - Returns the coupling element (index1, index2), where these are each others neighbour 2
4. Description
   - The overload of the functional is done to avoid branching. When building the matrix, one calls
the individual functions correctly to accomodate the correct neighbours.
5. Dependents
   - `System`, but only virtually
6. Typical dependencies
   - `params_t`
   - `vars_t`
   - `drum_t` (existence of `get_parameters()` and `get_variables()`)

<a name="rk4b"></a>
#### `RK4Buffer` (rk4_buffer.hpp), holds Rk4Stepper intermediate results
1. Description
   - This class must never be changed except when changing stepper

<a name="rk4s"></a>
#### `Rk4Stepper` (rk4_stepper.hpp), performs timesteps using rk4 scheme
1. Template arguments
   - `value_t`: Scalar type
   - `params_t`: Drum parameters container type
   - `vars_t`: Drum variables container type
   - `buffer_t`: Stepper buffer container type
   - `force_t`: Force functional type
2. Explicit constructors
   - `Rk4Stepper()`
3. Member functions
   - `void step_1(force_t, std::vector<drum_t>, std::vector<std::vector<int> >, value_t dt, value_t time)`
   - `void step_2(force_t, std::vector<drum_t>, std::vector<std::vector<int> >, value_t dt, value_t time)`
   - `void step_3(force_t, std::vector<drum_t>, std::vector<std::vector<int> >, value_t dt, value_t time)`
     - All of the above perform one step of a timestep, between successive steps certain
     other updates need to be taken care of by the [owner object](#system).
     - Arguments: Force functional, Drum vector, Adjacency vector, time step, start time of current step
4. Dependents
   - `System`
4. Dependencies
   - `force_t`, but only virtually
   - `buffer_t`
   - `Drum`
   - `vars_t`, see [DrumVariables](#drumvars)

<a name="sysparams"></a>
#### `SystemParameters` (system_parameters.hpp), holds system parameters
1. Template arguments
   - `coupler_t`: Coupler type
   - `driver_t`: Driver type
2. Explicit constructors
   - `SystemParameters(coupler_t, driver_t, std::vector<std::vector<int> >)`
3. Public data members
   - `coupler`: The coupler_t object of the system
   - `driver`: The driver_t object of the system
   - `adjacency_vector`: A `std::vector<std::vector<int> >` representing the adjacency vector
4. Dependents
   - `System`
5. Dependencies
   - None

<a name="grabber"></a>
#### `Grabber` (grabber.hpp), grabs and saves data
1. Template arguments
   - `value_t`: Scalar type
   - `drum_t`: Drum type
2. Explicit constructors
   - `Grabber(const size_t grab_every, const std::string params_file, const std::string adjacency_file, const std::string dynamic_file)`
     - `grab_every`: stride between data grabs
     - `params_file`: file for parameters saving
     - `adjacency_file`: file for lattice adjacency saving
     - `dynamic_file:` file for dynamics saving
   - `Grabber() = delete`
   - `Grabber(const Grabber&)`
3. Member functions
   - `void init(value_t t_end, value_t dt, std::vector<drum_t>, std::vector<std::vector<int> >)`
     - Called upon `System` lifetime start.
   - `bool grab(std::vector<drum_t>, value_t time)`
     - Grabs data from the drums.
     - May use current time to decide if to grab data.
     - Returns `true` if data was grabbed.
   - `bool save()`
     - Saves data to the specified files.
     - Is called by `System`.
     - Returns `true` on success.
4. Dependents
   - `System`
5. Dependencies
   - `Drum`
   - `DrumParameters`
   - `DrumVariables`
6. Description
   - This class needs to be re-implemented whenever either `DrumParameters` or
   `DrumVariables` changes.
   - One should also implement a helper struct, as seen in `include/grabber.hpp`

<a name="system"></a>
#### `System` (system.hpp), holds all parts together
1. Template arguments
   - `value_t`: Scalar type
   - `drum_t`: Drum type
   - `grabber_t`: Data exfiltrator type
   - `sysparams_t`: System parameters type
   - `force_t`: Force functional type
   - `coupler_t`: Coupler type
   - `driver_t`: Driver type
   - `stepper_t`: Stepper type
   - `matelecalc_t`: Matrix element calculator type
2. Explicit constructors
   - `System(const value_t t_end, const value_t dt, const std::vector<drum_t>& ds, const stepper_t s, const force_t f, const sysparams_t sp, const grabber_t g)`
     - `t_end`: Temporal simulation length
     - `dt`: Simulation time step
     - `ds`: Vector of drums
     - `s`: Time stepper
     - `f`: Force functional
     - `sp`: System parameters
     - `g`: Grabber
3. Member functions
   - `void simulate()`
     - Runs a simulation with the set parameters.
   - `void step()`
     - Performs one timestep with the set parameters.
   - `void reset_time()`
     - Resets the simulation time to zero.
   - `void set_step(value_t)`
     - Sets the timestep.
   - `bool save()`
     - Calls `grabber_t::save()`.
   - `std::vector<value_t> get_matrix(matelecalc_t)`
     - Returns the dynamic matrix as provided by the argument.
4. Dependents
   - None
5. Dependencies
   - All of them. Don't change this class before thinking a lot.
