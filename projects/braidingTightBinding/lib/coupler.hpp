#ifndef COUPLER_HPP_INCLUDED
#define COUPLER_HPP_INCLUDED
#include <vector>

template <typename value_t, typename drum_t>
class Coupler{
public:
  //constructors
  Coupler() = default;
  ~Coupler() = default;

  virtual void precompute(const value_t t_end, const value_t dt, const std::vector<drum_t>& drum_vec) noexcept = 0;
  virtual void step(value_t dt) noexcept = 0;
  virtual value_t operator()(const size_t drum_index, const size_t neighbour_index) const noexcept = 0;
};

#endif
