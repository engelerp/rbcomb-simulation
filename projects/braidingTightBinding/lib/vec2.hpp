#ifndef VEC2_HPP_INCLUDED
#define VEC2_HPP_INCLUDED
#include <cmath>
#include <iostream>

template <typename value_t>
class Vec2{
public:
  //constructors
  Vec2(const value_t x, const value_t y) noexcept: x_(x), y_(y) {}
  Vec2(const Vec2<value_t>& other) = default;
  Vec2& operator=(const Vec2&) = default;
  Vec2() = default;
  ~Vec2() = default;


  //access
  value_t x() const noexcept { return x_; }
  value_t y() const noexcept { return y_; }
  Vec2 normalized() const { return *this/this->norm(); }

  //TODO: Precalculate these and flag when recomputation is needed
  value_t r() const noexcept { return this->norm(); }
  value_t phi() const
  {
    value_t angle (0);
    angle = std::atan2(y_, x_);
    if(std::isnan(angle)){
      throw("ATAN2 NAN OCCURRED");
      return 0.;
    }
    return angle;
  }

  //function members
  value_t r_wrt(const Vec2& origin) const noexcept { return (*this - origin).r(); }
  value_t phi_wrt(const Vec2& origin) const { return (*this - origin).phi(); }

  value_t norm() const noexcept { return std::sqrt(x_*x_ + y_*y_); }
  value_t norm_sq() const noexcept { return x_*x_ + y_* y_; }

  //modifiers
  Vec2& rotate(const Vec2& center, const value_t degrees)
  {
    *this -= center;
    value_t temp = x_;
    const value_t radians = degrees*3.141592653589793/180.;

    x_ = std::cos(radians)*x_ - std::sin(radians)*y_;
    y_ = std::sin(radians)*temp + std::cos(radians)*y_;

    *this += center;

    return *this;
  }

  Vec2& normalize()
  {
    value_t length = norm();
    if(length == value_t(0))
            throw("NORMALIZE ZERO VEC2 ATTEMPTED");
    x_ /= length;
    y_ /= length;
    return *this;
  }

  //overloading
  Vec2& operator+=(const Vec2& rhs) noexcept
  {
    x_ += rhs.x_;
    y_ += rhs.y_;
    return *this;
  }
  Vec2& operator-=(const Vec2& rhs) noexcept
  {
    x_ -= rhs.x_;
    y_ -= rhs.y_;
    return *this;
  }
  Vec2& operator*=(const value_t s) noexcept
  {
    x_ *= s;
    y_ *= s;
    return *this;
  }
  Vec2& operator/=(const value_t s)
  {
    if(x_ == value_t(0) and y_ == value_t(0))
      return *this;
    else if(s == value_t(0))
      throw("DIVISION BY ZERO ATTEMPTED");
    x_ /= s;
    y_ /= s;
    return *this;
  }

  value_t& operator[](std::size_t i) noexcept
  {
    if(i == 0)
      return x_;
    else
      return y_;
  }
  value_t operator[](std::size_t i) const noexcept
  {
    if(i == 0)
      return x_;
    else
      return y_;
  }

  //non-ADL hidden friends
  friend Vec2 operator+(const Vec2& lhs, Vec2 rhs) noexcept
  {
    rhs += lhs;
    return rhs;
  }
  friend Vec2 operator-(Vec2 lhs, const Vec2& rhs) noexcept
  {
    lhs -= rhs;
    return lhs;
  }
  friend Vec2 operator*(const value_t s, Vec2 v) noexcept
  {
    v *= s;
    return v;
  }
  friend Vec2 operator*(Vec2 v, const value_t s) noexcept
  {
    v *= s;
    return v;
  }
  friend Vec2 operator/(Vec2 v, const value_t s)
  {
    v /= s;
    return v;
  }
  friend value_t operator*(const Vec2& v, const Vec2& w) noexcept { return v.x_*w.x_ + v.y_*w.y_; }//dot product

private:
  value_t x_, y_;
};

template <typename value_t>
std::ostream& operator<<(std::ostream& os, const Vec2<value_t>& v)
{
  return os << "(" << v.x() << ", " << v.y() << ")";
}

#endif
