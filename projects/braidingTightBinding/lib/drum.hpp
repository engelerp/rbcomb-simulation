#ifndef DRUM_HPP_INCLUDED
#define DRUM_HPP_INCLUDED
#include <cmath>

/* Neighbour enumeration:
 * For drums in sublattice 'A', neighbour 0 is straight down, then clockwise.
 * For drums in sublattice 'B', neighbour 0 is straight up, then clockwise.
 * Hence neighbouring drums see each other as the same number neighbour.
 */

template <typename value_t, typename params_t, typename vars_t, typename sbuffer_t>
class Drum{
public:
  //'structors
  Drum(const params_t& dp) noexcept: params_(dp) {}
  Drum() = delete; //default construction not allowed
  Drum(const Drum&) = default;
  Drum& operator=(const Drum&) = default;
  ~Drum() = default;

  //access
  params_t& get_parameters() noexcept
  {
    return params_;
  }

  const params_t& get_parameters() const noexcept
  {
    return params_;
  }

  vars_t& get_variables() noexcept
  {
    return vars_;
  }

  const vars_t& get_variables() const noexcept
  {
    return vars_;
  }

  sbuffer_t& get_sbuffer() noexcept
  {
    return sbuffer_;
  }

  const sbuffer_t& get_sbuffer() const noexcept
  {
    return sbuffer_;
  }

  //modifiers
  void set_coupling_0(value_t t0)
  {
    vars_.t0 = t0;
  }
  void set_coupling_1(value_t t1)
  {
    vars_.t1 = t1;
  }
  void set_coupling_2(value_t t2)
  {
    vars_.t2 = t2;
  }
  void set_drive(value_t V)
  {
    vars_.V = V;
  }

private:
  params_t params_; // constant drum parameters
  vars_t vars_; // drum variables
  sbuffer_t sbuffer_; //stepper buffer
};

#endif
