#ifndef DIAGONALIZER_HPP_INCLUDED
#define DIAGONALIZER_HPP_INCLUDED
#include <vector>
#include <iostream>
#include <utility>

extern "C" void dsyev_(
                char    const & JOBZ,   // 'N': Only eigenvalues, 'V': Eigenvalues and -vectors
                char    const & UPLO,   // 'U': Upper triangular, 'V': Lower triangular
                int     const & N,      // Matrix order
                double        * A,      // Matrix and Eigenvector output
                int     const & LDA,    // Matrix order (for my purposes)
                double        * W,      // Eigenvalue output
                double        * WORK,   // Workspace
                int     const & LWORK,  // Size of workspace
                int           & INFO    // Info
                );

//TODO: Rule of 7 conformity (nontrivial heap resource)
class Diagonalizer{
public:
  Diagonalizer() = default;
  ~Diagonalizer() = default;

  std::vector<double> ev(const std::vector<double>& matrix, const size_t N){
    //prepare memory
    matrix_ = matrix;
    if(eigenvalues_.size() != N){
      eigenvalues_.clear();
      eigenvalues_.reserve(N - eigenvalues_.capacity());
    }
    for(size_t i = 0; i < N; ++i){
      eigenvalues_.push_back(0.);
    }

    //prepare workspace
    N_ = N;
    prepare_workspace('N');

    dsyev_('N', 'L', N_, matrix_.data(), N_, eigenvalues_.data(), work_, lwork_, info_);

    if(info_ != 0){
      throw("Diagonalization failed!");
    }

    //clean up
    delete[] work_;

    return eigenvalues_;
  }

  std::pair<std::vector<double>, std::vector<double> > evv(const std::vector<double>& matrix, const size_t N){
    //prepare memory
    matrix_ = matrix;
    if(eigenvalues_.size() != N){
      eigenvalues_.clear();
      eigenvalues_.reserve(N - eigenvalues_.capacity());
    }
    for(size_t i = 0; i < N; ++i){
      eigenvalues_.push_back(0.);
    }

    //prepare workspace
    N_ = N;
    prepare_workspace('V');

    dsyev_('V', 'L', N_, matrix_.data(), N_, eigenvalues_.data(), work_, lwork_, info_);

    if(info_ != 0){
      throw("Diagonalization failed!");
    }

    //clean up
    delete[] work_;

    return std::pair<std::vector<double>, std::vector<double> > (eigenvalues_, matrix_);
  }

private:
  void prepare_workspace(char type){
    dsyev_(type, 'L', N_, matrix_.data(), N_, eigenvalues_.data(), &dwork_, -1, info_);
    lwork_ = static_cast<int>(dwork_);
    work_ = new double[lwork_];
  }

  int info_;
  double dwork_;
  int lwork_;
  int N_; //linear matrix dimension
  std::vector<double> matrix_; //the matrix
  std::vector<double> eigenvalues_; //eigenvalues
  double* work_;
};

#endif
