#ifndef DRIVER_BRAID_HPP_INCLUDED
#define DRIVER_BRAID_HPP_INCLUDED
#include <vector>
#include <drum.hpp>

template <typename value_t, typename drum_t>
class DriverBraid: public Driver<value_t, drum_t>{
public:
  //constructors
  DriverBraid() = default;
  ~DriverBraid() = default;

  void precompute(const value_t t_end, const value_t dt, const std::vector<drum_t>& drum_vec) noexcept final override
  {

  }
  void step(value_t dt) noexcept final
  {

  }
  value_t operator()(const size_t drum_index) const noexcept final override
  {
    return 0;
  }
};

#endif
