#ifndef DRUM_PARAMETERS_BRAID_HPP_INCLUDED
#define DRUM_PARAMETERS_BRAID_HPP_INCLUDED
#include <vec2.hpp>
#include <cmath>
#include <iostream>

//TODO: Create an interface. It would be a trivial one, though.
template <typename value_t>
class DrumParametersBraid{
public:
  /* Arguments:
   * k0:                  diagonal matrix element
   * k1:                  coupling matrix element prefactor 1
   * k2:                  coupling matrix element prefactor 2
   * c:                   damping coefficient
   * position:            position of the drum in real space
   * sublattice:          'A'/'a' or 'B'/'b', identifies sublattice drum is part of
   */
  DrumParametersBraid(const value_t k0, const value_t k1, const value_t k2, const value_t c, const Vec2<value_t>& position, char sublattice) noexcept
          : k0(k0), k1(k1), k2(k2), c(c), position(position), sublattice(sublattice)
  {
    generate_sublattice(sublattice, value_t(1.));
  }
  DrumParametersBraid() = delete; //no default initialization allowed
  DrumParametersBraid(const DrumParametersBraid&) = default;
  DrumParametersBraid& operator=(const DrumParametersBraid&) = default;
  ~DrumParametersBraid() = default;

private:
  int generate_sublattice(char sublattice, value_t lc) noexcept{
    if(sublattice == 'A' or sublattice == 'a'){
      s0 = Vec2<value_t> (0, -lc);
      s1 = Vec2<value_t> (-std::sqrt(3)*lc/2., lc/2.);
      s2 = Vec2<value_t> (std::sqrt(3)*lc/2., lc/2.);
      return 0;
    }
    else if(sublattice == 'B' or sublattice == 'b'){
      s0 = -1*(Vec2<value_t> (0, -lc));
      s1 = -1*(Vec2<value_t> (-std::sqrt(3)*lc/2., lc/2.));
      s2 = -1*(Vec2<value_t> (std::sqrt(3)*lc/2., lc/2.));
      return 0;
    }
    else{
      return -1; //invalid sublattice identifier
    }
  }

public:
  value_t k0; //onsite prefactor
  value_t k1; //coupling prefactor 1
  value_t k2; //coupling prefactor 2
  value_t c; //damping coefficient
  char sublattice; //sublattice the drum is part of (governs electrode geometry)
  Vec2<value_t> s0, s1, s2; //vectors connecting to neighbours, ordered according to drum.hpp
  Vec2<value_t> position; //position of the drum in real space

};

template<typename value_t>
std::ostream& operator<<(std::ostream& os, const DrumParametersBraid<value_t>& dp)
{
  return os << "Sublattice: " << dp.sublattice << ", k0: " << dp.k0 << ", k1: " << dp.k1 << ", k2: " << dp.k2 << std::endl << "s0: " << dp.s0 << ", s1: " << dp.s1 << ", s2: " << dp.s2;

}

#endif
