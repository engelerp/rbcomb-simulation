#ifndef COUPLER_CONST_HPP_INCLUDED
#define COUPLER_CONST_HPP_INCLUDED
#include <vector>
#include <coupler.hpp>

template <typename value_t, typename drum_t, typename params_t>
class CouplerConst: public Coupler<value_t, drum_t>{
public:
  //constructors
  CouplerConst(value_t value): value_(value) {}
  ~CouplerConst() = default;

  void precompute(const value_t t_end, const value_t dt, const std::vector<drum_t>& drum_vec) noexcept final override
  {
    //nothing to do
  }
  void step(value_t dt) noexcept final
  {
    //nothing to do
  }
  value_t operator()(const size_t drum_index, const size_t neighbour_index) const noexcept final override
  {
    return value_;
  }

private:
  value_t value_;
};

#endif
