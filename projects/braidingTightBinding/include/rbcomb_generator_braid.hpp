#ifndef RBCOMB_GENERATOR_BRAID_HPP_INCLUDED
#define RBCOMB_GENERATOR_BRAID_HPP_INCLUDED
#include <lattice_generator.hpp>
#include <vec2.hpp>
#include <utility>
#include <vector>

template <typename value_t, typename params_t, typename vars_t, typename sbuffer_t>
class RbcombGeneratorBraid: public LatticeGenerator<value_t, params_t, vars_t, sbuffer_t> {
public:
  RbcombGeneratorBraid(size_t x_extent, size_t y_extent) noexcept : x_extent_(x_extent), y_extent_(y_extent) {}
  RbcombGeneratorBraid() = delete;
  ~RbcombGeneratorBraid() = default;

  //the params_t argument shall contain the position of the first drum that is placed.
  //the last drum (return.first.back()) signifies inexistent neighbours, it is not part of the lattice.
  //It shall be of sublattice 'A'.
  std::pair<std::vector<Drum<value_t, params_t, vars_t, sbuffer_t> >, std::vector<std::vector<int> > >
  operator()(const params_t& drum_params) noexcept final override
  {
    using drum_t = Drum<value_t, params_t, vars_t, sbuffer_t>;
    std::vector<drum_t> current_line, next_line;
    current_line.reserve(x_extent_);
    next_line.reserve(x_extent_);

    //calculate vectors to next drums
    Vec2<value_t> vec_right = drum_params.s2 - drum_params.s1; //drum to the right in same row
    Vec2<value_t> vec_down = drum_params.s0; //drum below
    Vec2<value_t> vec_upleft = drum_params.s1; //drum at top left
    Vec2<value_t> vec_upright = drum_params.s2; //drum at top right

    //prepare memory
    drums_.reserve(x_extent_);
    adjacency_vector_.reserve(x_extent_);
    //push first drum
    drums_.push_back(drum_t(drum_params));
    adjacency_vector_.push_back(std::vector<int>(3,-1)); //initialized with 'no neighbour here'

    //initialize the first line to bootstrap the algorithm
    for(size_t i = 1; i < x_extent_; ++i){
      params_t new_params (drum_params);
      new_params.position = drums_.back().get_parameters().position + vec_right;
      drums_.push_back(drum_t(new_params));
      adjacency_vector_.push_back(std::vector<int>(3,-1));
    }

    //add y_extent_ inequivalent hexagon rows
    for(size_t i = 0; i < y_extent_; ++i){
      generate_layer(vec_down, vec_upleft, vec_upright);
    }

    //terminate by adding another B-row
    terminate_lattice(vec_upleft, vec_upright);

    //push a drum to the end that has no neighbours and signifies the inexistent neighbour
    drums_.push_back(drum_t(drum_params));
    adjacency_vector_.push_back(std::vector<int>(3,-1));
    //make no-neighbours point here
    for(size_t i = 0; i < adjacency_vector_.size()-1; ++i){
      for(size_t j = 0; j < 3; ++j){
        if(adjacency_vector_[i][j] == -1){
          adjacency_vector_[i][j] = drums_.size()-1;
        }
      }
    }

    /* Deploy without consistency check
    if(check_consistency() == false)
    std::cout << "INCONSISTENT LATTICE DETECTED!\n";
    */

    return std::pair<std::vector<drum_t>, std::vector<std::vector<int> > > (drums_, adjacency_vector_);
  }

private:
  //check if there exists a drum in drum_vec that is close to drum_pos.
  //returns the index of the first close drum or -1.
  int find_close(
  const Vec2<value_t>& drum_pos,
  const std::vector<Drum<value_t, params_t, vars_t, sbuffer_t> >& drum_vec
  ) const noexcept
  {
    for(int i = 0; i < drum_vec.size(); ++i){
      auto test_drum = drum_vec[i];
      if(test_drum.get_parameters().position.r_wrt(drum_pos) < 1.e-8){
        return i;
      }
    }
    return -1; //no close drum found
  }

  //checks sanity of generated lattice
  //returns true if lattice passed the test
  bool check_consistency() noexcept
  {
    //check for duplicate drums
    for(size_t i = 0; i < drums_.size(); ++i){
      if(i != find_close(drums_[i].get_parameters().position, drums_)){
        return false;
      }
    }
    return true;
  }

  //arguments are the neighbour vectors down, upleft, upright.
  //adds a new layer of drums to drums_, filling in the adjency_vector_ as well.
  //
  // 4:            |      x_extent_ drums
  // 3:           \/      x_extent_ + 1 drums
  // 2:           |       x_extent_ + 1 drums
  // 1:            \/     x_extent_ drums
  void generate_layer(const Vec2<value_t>& v_d, const Vec2<value_t>& v_ul, const Vec2<value_t>& v_ur) noexcept
  {
    //shorten syntax
    using drum_t = Drum<value_t, params_t, vars_t, sbuffer_t>;

    //prepare vector to hold new row
    std::vector<drum_t> new_row;
    new_row.reserve(x_extent_);

    //prepare parameter templates for both sublattices
    params_t params_A(drums_[0].get_parameters()); //first drum will always be of sublattice 'A'
    params_t params_B(params_A.k0, params_A.k1, params_A.k2, params_A.c, params_A.position, 'B'); //generate sublattice 'B' type of parameters

    //row 1
    //prepare iterator
    auto drum_it = drums_.end();
    drum_it -= static_cast<int>(x_extent_);

    //first drum is special
    Vec2<value_t> new_pos_first = drum_it->get_parameters().position + v_ur;
    params_t new_params_first (params_B);
    new_params_first.position = new_pos_first;
    new_row.push_back(drum_t(new_params_first));
    int index_1 = drums_.size() + new_row.size()-1;
    adjacency_vector_.push_back(std::vector<int>(3,-1));
    adjacency_vector_[std::distance(drums_.begin(), drum_it)][2] = drums_.size();
    adjacency_vector_[drums_.size()][2] = std::distance(drums_.begin(), drum_it);
    ++drum_it;

    //now all other drums
    while(drum_it != drums_.end()){
      Vec2<value_t> new_pos1 = drum_it->get_parameters().position + v_ul;
      Vec2<value_t> new_pos2 = drum_it->get_parameters().position + v_ur;

      //see if these drums already exist and add them to the list
      int index_1 = find_close(new_pos1, new_row);
      if(index_1 == -1){ //not in there yet, so push it
        params_t new_params (params_B);
        new_params.position = new_pos1;
        new_row.push_back(drum_t(new_params));
        index_1 = drums_.size()+new_row.size()-1;
        //make adjacency vector longer
        adjacency_vector_.push_back(std::vector<int>(3,-1));
      }
      else{
        index_1 += drums_.size();
      }
      int index_2 = find_close(new_pos2, new_row);
      if(index_2 == -1){ //not in there yet, so push it
        params_t new_params (params_B);
        new_params.position = new_pos2;
        new_row.push_back(drum_t(new_params));
        index_2 = drums_.size()+new_row.size()-1;
        //make adjacency vector longer
        adjacency_vector_.push_back(std::vector<int>(3,-1));
      }
      else{
        index_2 += drums_.size();
      }
      //add new bonds to the adjacency vector
      int old_index = std::distance(drums_.begin(), drum_it);
      adjacency_vector_[old_index][1] = index_1;
      adjacency_vector_[old_index][2] = index_2;
      adjacency_vector_[index_1][1] = old_index;
      adjacency_vector_[index_2][2] = old_index;

      ++drum_it;
    }

    //push new drums
    for(auto d: new_row){
      drums_.push_back(d);
    }

    //clear new_row
    new_row.clear();

    //row 2
    //prepare iterator
    drum_it = drums_.end();
    drum_it -= static_cast<int>(x_extent_);

    //first drum is special, we add one to the left
    new_params_first = params_A;
    new_params_first.position = drum_it->get_parameters().position - v_d + v_ul - v_ur;
    new_row.push_back(drum_t(new_params_first)); //this drum has no neighbours yet.
    adjacency_vector_.push_back(std::vector<int>(3,-1));
    //we don't increment the iterator, as we reuse this drum for straight up addition

    while(drum_it != drums_.end()-1){
      //These drums are guaranteed to be unique, hence inexistent.
      params_t new_params (params_A);
      new_params.position = drum_it->get_parameters().position - v_d;
      new_row.push_back(drum_t(new_params));
      int index = drums_.size() + new_row.size() - 1;
      adjacency_vector_.push_back(std::vector<int>(3,-1));

      int old_index = std::distance(drums_.begin(), drum_it);
      adjacency_vector_[old_index][0] = index;
      adjacency_vector_[index][0] = old_index;

      ++drum_it;
    }

    //push new drums
    for(auto d: new_row){
      drums_.push_back(d);
    }

    //clear new_row
    new_row.clear();

    //row 3
    drum_it = drums_.end();
    drum_it -= static_cast<int>(x_extent_); //this row has one drum more

    //first drum is special (only goes to ur)
    params_t new_params (params_B);
    new_params.position = drum_it->get_parameters().position + v_ur;
    new_row.push_back(drum_t(new_params));
    int indexx = drums_.size();
    adjacency_vector_.push_back(std::vector<int>(3,-1));
    int old_indexx = std::distance(drums_.begin(), drum_it); //avoid naming conflicts
    adjacency_vector_[old_indexx][2] = indexx;
    adjacency_vector_[indexx][2] = old_indexx;
    ++drum_it;

    //now the unspecial ones
    while(drum_it != drums_.end()){
      Vec2<value_t> new_pos1 = drum_it->get_parameters().position + v_ul;
      Vec2<value_t> new_pos2 = drum_it->get_parameters().position + v_ur;

      //see if these drums already exist and add them to the list
      int index_1 = find_close(new_pos1, new_row);
      if(index_1 == -1){ //not in there yet, so push it
        params_t new_params (params_B);
        new_params.position = new_pos1;
        new_row.push_back(drum_t(new_params));
        index_1 = drums_.size()+new_row.size()-1;
        //make adjacency vector longer
        adjacency_vector_.push_back(std::vector<int>(3,-1));
      }
      else{
        index_1 += drums_.size();
      }
      int index_2 = find_close(new_pos2, new_row);
      if(index_2 == -1){ //not in there yet, so push it
        params_t new_params (params_B);
        new_params.position = new_pos2;
        new_row.push_back(drum_t(new_params));
        index_2 = drums_.size()+new_row.size()-1;
        //make adjacency vector longer
        adjacency_vector_.push_back(std::vector<int>(3,-1));
      }
      else{
        index_2 += drums_.size();
      }

      //add new bonds to the adjacency vector
      int old_index = std::distance(drums_.begin(), drum_it);
      adjacency_vector_[old_index][1] = index_1;
      adjacency_vector_[old_index][2] = index_2;
      adjacency_vector_[index_1][1] = old_index;
      adjacency_vector_[index_2][2] = old_index;

      ++drum_it;
    }

    //push new drums
    for(auto d: new_row){
      drums_.push_back(d);
    }

    //clear new_row
    new_row.clear();



    //row 4
    //prepare iterator
    drum_it = drums_.end();
    drum_it -= static_cast<int>(x_extent_);

    while(drum_it != drums_.end()){
      //These drums are guaranteed to be unique, hence inexistent.
      params_t new_params (params_A);
      new_params.position = drum_it->get_parameters().position - v_d;
      new_row.push_back(drum_t(new_params));
      int index = drums_.size() + new_row.size() - 1;
      adjacency_vector_.push_back(std::vector<int>(3,-1));

      int old_index = std::distance(drums_.begin(), drum_it);
      adjacency_vector_[old_index][0] = index;
      adjacency_vector_[index][0] = old_index;

      ++drum_it;
    }

    //push new drums
    for(auto d: new_row){
      drums_.push_back(d);
    }

    //clear new_row
    new_row.clear();
  }

  void terminate_lattice(const Vec2<value_t>& v_ul, const Vec2<value_t>& v_ur) noexcept {
    //shorten syntax
    using drum_t = Drum<value_t, params_t, vars_t, sbuffer_t>;

    //prepare vector to hold new row
    std::vector<drum_t> new_row;
    new_row.reserve(x_extent_);

    //prepare parameter templates for both sublattices
    params_t params_A(drums_[0].get_parameters()); //first drum will always be of sublattice 'A'
    params_t params_B(params_A.k0, params_A.k1, params_A.k2, params_A.c, params_A.position, 'B'); //generate sublattice 'B' type of parameters

    //row 1
    //prepare iterator
    auto drum_it = drums_.end();
    drum_it -= static_cast<int>(x_extent_);

    //first drum is special
    Vec2<value_t> new_pos_first = drum_it->get_parameters().position + v_ur;
    params_t new_params_first (params_B);
    new_params_first.position = new_pos_first;
    new_row.push_back(drum_t(new_params_first));
    int index_1 = drums_.size() + new_row.size()-1;
    adjacency_vector_.push_back(std::vector<int>(3,-1));
    adjacency_vector_[std::distance(drums_.begin(), drum_it)][2] = drums_.size();
    adjacency_vector_[drums_.size()][2] = std::distance(drums_.begin(), drum_it);
    ++drum_it;

    //now all other drums
    while(drum_it != drums_.end()){
      Vec2<value_t> new_pos1 = drum_it->get_parameters().position + v_ul;
      Vec2<value_t> new_pos2 = drum_it->get_parameters().position + v_ur;

      //see if these drums already exist and add them to the list
      int index_1 = find_close(new_pos1, new_row);
      if(index_1 == -1){ //not in there yet, so push it
        params_t new_params (params_B);
        new_params.position = new_pos1;
        new_row.push_back(drum_t(new_params));
        index_1 = drums_.size()+new_row.size()-1;
        //make adjacency vector longer
        adjacency_vector_.push_back(std::vector<int>(3,-1));
      }
      else{
        index_1 += drums_.size();
      }
      int index_2 = find_close(new_pos2, new_row);
      if(index_2 == -1){ //not in there yet, so push it
        params_t new_params (params_B);
        new_params.position = new_pos2;
        new_row.push_back(drum_t(new_params));
        index_2 = drums_.size()+new_row.size()-1;
        //make adjacency vector longer
        adjacency_vector_.push_back(std::vector<int>(3,-1));
      }
      else{
        index_2 += drums_.size();
      }

      //add new bonds to the adjacency vector
      int old_index = std::distance(drums_.begin(), drum_it);
      adjacency_vector_[old_index][1] = index_1;
      adjacency_vector_[old_index][2] = index_2;
      adjacency_vector_[index_1][1] = old_index;
      adjacency_vector_[index_2][2] = old_index;

      ++drum_it;
    }

    //push new drums
    for(auto d: new_row){
      drums_.push_back(d);
    }

  }

  size_t x_extent_, y_extent_; //Number of equivalent hexagon rows in x and y direction
  std::vector<Drum<value_t, params_t, vars_t, sbuffer_t> > drums_;
  std::vector<std::vector<int> > adjacency_vector_;
};

#endif
