#ifndef DRUM_VARIABLES_BRAID_HPP_INCLUDED
#define DRUM_VARIABLES_BRAID_HPP_INCLUDED

/*
 * Stores all dynamic variables of a drum
 */

//TODO: Generate an interface. It would be trivial, though.
template <typename value_t>
class DrumVariablesBraid{
public:
  //constructors
  DrumVariablesBraid() = default;
  DrumVariablesBraid& operator=(const DrumVariablesBraid&) = default;
  ~DrumVariablesBraid() = default;

  value_t t0, t1, t2; //t + dt, i.e. baseline plus vortices along bond 0,1,2
  value_t V; //Coupling to central electrode
  value_t x, xdot; //Current Elongation (position) and velocity
  value_t x_temp, xdot_temp; //used as calculation arguments in rk steps
};

#endif
