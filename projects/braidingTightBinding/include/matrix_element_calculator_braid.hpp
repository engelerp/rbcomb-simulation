#ifndef MATRIX_ELEMENT_CALCULATOR_BRAID_HPP_INCLUDED
#define MATRIX_ELEMENT_CALCULATOR_BRAID_HPP_INCLUDED
#include <matrix_element_calculator.hpp>

template <typename value_t, typename params_t, typename vars_t, typename drum_t>
class MatrixElementCalculatorBraid: public MatrixElementCalculator<value_t, params_t, vars_t, drum_t>{
public:
  MatrixElementCalculatorBraid() = default;
  ~MatrixElementCalculatorBraid() = default;

  //diagonal element at (index, index)
  value_t operator()(const size_t index, const std::vector<drum_t>& drums) const noexcept final override
  {
          return drums[index].get_parameters().k0;
  }

  //coupling elements at (index1, index2)
  //for neighbour 0
  value_t operator()(const size_t index1, const size_t index2, const std::vector<drum_t>& drums) const noexcept final override
  {
          return drums[index1].get_parameters().k1 + drums[index1].get_parameters().k2 * drums[index2].get_variables().t0;
  }
  //for neighbour 1
  value_t operator()(const size_t index1, const size_t index2, const std::vector<drum_t>& drums, const int) const noexcept final override
  {
          return drums[index1].get_parameters().k1 + drums[index1].get_parameters().k2 * drums[index2].get_variables().t1;
  }
  //for neighbour 2
  value_t operator()(const size_t index1, const size_t index2, const std::vector<drum_t>& drums, const int, const int) const noexcept final override
  {
          return drums[index1].get_parameters().k1 + drums[index1].get_parameters().k2 * drums[index2].get_variables().t2;
  }
};

#endif
