#ifndef GRABBER_HPP_INCLUDED
#define GRABBER_HPP_INCLUDED
#include <vector>
#include <list>
#include <fstream>
#include <string>

template<typename value_t>
struct DataStorage{
  DataStorage(const size_t num_drums)
  {
    x.reserve(num_drums);
    xdot.reserve(num_drums);
    t0.reserve(num_drums);
    t1.reserve(num_drums);
    t2.reserve(num_drums);
    drive.reserve(num_drums);
  }

  ~DataStorage() = default;

  value_t time;
  std::vector<value_t> x;
  std::vector<value_t> xdot;
  std::vector<value_t> t0;
  std::vector<value_t> t1;
  std::vector<value_t> t2;
  std::vector<value_t> drive;
};

template <typename value_t, typename drum_t>
class Grabber{
public:
  Grabber(const size_t grab_every, const std::string params_file, const std::string adjacency_file, const std::string dynamic_file)
          : grab_every_(grab_every), grab_cnt_(0), par_f_(params_file), adj_f_(adjacency_file), dyn_f_(dynamic_file) {}
  Grabber() = delete; //no default initialization
  Grabber(const Grabber&) = default;
  ~Grabber() = default;

  //call in system constructor
  void init(const value_t t_end, const value_t dt, const std::vector<drum_t>& drums, const std::vector<std::vector<int> >& adjacency) noexcept
  {
    drums_ = drums;
    adjacency_ = adjacency;
    t_end_ = t_end;
    dt_ = dt;
  }

  //call after each simulation step, checks itself if it should save
  bool grab(const std::vector<drum_t>& drums, const value_t time)
  {
    if(grab_cnt_++ % grab_every_ == 0){
      data_.push_back(DataStorage<value_t>(drums_.size()));
      data_.back().time = time;
      for(auto d: drums){
        data_.back().x.push_back(d.get_variables().x);
        data_.back().xdot.push_back(d.get_variables().xdot);
        data_.back().t0.push_back(d.get_variables().t0);
        data_.back().t1.push_back(d.get_variables().t1);
        data_.back().t2.push_back(d.get_variables().t2);
        data_.back().drive.push_back(d.get_variables().V);
      }
      return true;
    }
    else{
      return false;
    }
  }

  //prints the data out. call at end of it all.
  bool save(){
    //TODO: Check for file open
    //TODO: Overload operator<< for DrumParameters and DataStorage.
    //print parameters
    std::fstream par (par_f_, par.out);
    par << "# The last drum is to be ignored\n";
    par << "# k0 \t k1 \t k2 \t c \t pos_x \t pos_y \t sublattice\n";
    for(auto d: drums_){
      par << d.get_parameters().k0 << " \t";
      par << d.get_parameters().k1 << " \t";
      par << d.get_parameters().k2 << " \t";
      par << d.get_parameters().c << " \t";
      par << d.get_parameters().position.x() << " \t";
      par << d.get_parameters().position.y() << " \t";
      par << d.get_parameters().sublattice << "\n";
    }
    par.close();

    //print adjacency vectors
    std::fstream adj (adj_f_, adj.out);
    adj << "# <-1> means <no neighbour here>\n";
    adj << "# The last drum is to be ignored\n";
    adj << "# n0 \t n1 \t n2\n";
    for(auto v: adjacency_){
      for(auto i: v){
        if(i == drums_.size()-1){
          adj << -1 << " \t"; //no neighbour present
        }
        else{
          adj << i << " \t"; //neighbour
        }
      }
      adj << "\n";
    }
    adj.close();

    std::fstream dyn (dyn_f_, dyn.out);
    dyn << "# If adjacency of a bond is -1 in " << adj_f_;
    dyn << ", the coupling is to be ignored\n";
    dyn << "# The last drum is to be ignored\n";
    dyn << "# Number of drums : " << drums_.size() << "\n";
    dyn << "# t \t drum_index \t x \t xdot \t t0 \t t1 \t t2 \t drive\n";
    for(auto entry: data_){
      for(size_t i = 0; i < entry.x.size(); ++i){
        dyn << entry.time << " \t";
        dyn << i << " \t";
        dyn << entry.x[i] << " \t";
        dyn << entry.xdot[i] << " \t";
        dyn << entry.t0[i] << " \t";
        dyn << entry.t1[i] << " \t";
        dyn << entry.t2[i] << " \t";
        dyn << entry.drive[i] << "\n";
      }
    }
    dyn.close();

    return true;
  }

private:
  size_t grab_every_; //grab data every grab_every_ steps
  size_t grab_cnt_; //grab count
  value_t t_end_, dt_; //maximal simulation time and timestep
  std::vector<std::vector<int> > adjacency_; //adjacency vector
  std::vector<drum_t> drums_; //drums in initial configuration, grab params from here
  std::list<DataStorage<value_t> > data_; //here goes the dynamical data
  //filenames
  std::string par_f_; //parameters file
  std::string adj_f_; //adjacency file
  std::string dyn_f_; //dynamics file (this is the gold)
};

#endif
