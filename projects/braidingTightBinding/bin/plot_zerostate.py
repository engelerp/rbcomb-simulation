import matplotlib.pyplot as plt
import numpy as np

readpath = "../results/data/zerostate/"
writepath = "../results/plots/zerostate/"

#eigenvalues
ev = np.loadtxt(readpath+"eigenvalues.txt")

starting_mode = 1110
end_mode = 1180

plt.plot(np.array(list(range(starting_mode,end_mode))),ev[starting_mode:end_mode],'o', ms=1)
plt.title("Frequency levels under vortex Kekule")
plt.xlabel("Level index")
plt.ylabel("Frequency [Hz]")
plt.savefig(writepath + "Frequencies_zoomed.pdf", dpi = 500)

plt.clf()
plt.plot(ev[:],'o', ms=1)
plt.title("Frequency levels under vortex Kekule")
plt.xlabel("Level index")
plt.ylabel("Frequency [Hz]")
plt.savefig(writepath + "Frequencies.pdf", dpi = 500)




#eigenvectors
data = np.loadtxt(readpath+"eigenvectors.txt")
pos_data = np.loadtxt(readpath+"PARAMS.txt", dtype=str) #positions are rows 4 and 5
desired_length = len(data[0,:])
pos_x = pos_data[0:desired_length,4].astype('float')
pos_y = pos_data[:desired_length,5].astype('float')

plt.figure(figsize=(8,300))
for i in range(starting_mode,end_mode):
    plt.subplot(int(end_mode-starting_mode),1,i-starting_mode+1)
    cmax = np.amax(data[i,:])
    cmin = np.amin(data[i,:])
    if(-cmin > cmax):
        cmax = -cmin
    elif(-cmin <= cmax):
        cmin = -cmax
    plt.scatter(pos_x, pos_y, c=data[i,:], edgecolors='k', linewidth=0.1, cmap='seismic', s=16, vmin=cmin, vmax=cmax)
    plt.colorbar()
    plt.title(f"Mode {i+1}, ({ev[i]} Hz)")
    plt.xlabel("x")
    plt.ylabel("y")

plt.savefig(writepath + f"Modes.pdf", dpi = 40, bbox_inches='tight', pad_inches=0)
