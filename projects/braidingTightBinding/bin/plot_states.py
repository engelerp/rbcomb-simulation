import matplotlib.pyplot as plt
import numpy as np
import sys

readpath = "../results/data/zerostate/"
writepath = "../results/plots/zerostate/"

if(len(sys.argv) < 2):
    print(f"Usage: python3.9 {sys.argv[0]} mode1 mode2 mode3 ...")

else:
    num_modes = int(len(sys.argv)-1)
    #eigenvalues
    ev = np.loadtxt(readpath+"eigenvalues.txt")
    #eigenvectors
    data = np.loadtxt(readpath+"eigenvectors.txt")
    pos_data = np.loadtxt(readpath+"PARAMS.txt", dtype=str) #positions are rows 4 and 5
    desired_length = len(data[0,:])
    pos_x = pos_data[0:desired_length,4].astype('float')
    pos_y = pos_data[:desired_length,5].astype('float')

    plt.figure(figsize=(8,5*num_modes))
    count = 1
    for str_i in sys.argv[1:]:
        i = int(str_i)-1
        plt.subplot(num_modes, 1, count)
        cmax = np.amax(data[i,:])
        cmin = np.amin(data[i,:])
        if(-cmin > cmax):
            cmax = -cmin
        elif(-cmin <= cmax):
            cmin = -cmax
        plt.scatter(pos_x, pos_y, c=data[i,:], edgecolors='k', linewidth=0.1, cmap='seismic', s=16, vmin=cmin, vmax=cmax)
        plt.colorbar()
        plt.title(f"Mode {i+1}, ({ev[i]} Hz)")
        plt.xlabel("x")
        plt.ylabel("y")
        count += 1

name = "Modes"
for str_i in sys.argv[1:]:
    name += f"_{str_i}"
plt.savefig(writepath + name, dpi=500, bbox_inches='tight', pad_inches=0)
